//
//  ViewController.swift
//  HowManyFingers
//
//  Created by Richmond Ko on 30/08/2016.
//  Copyright © 2016 Richmond Ko. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var userInput: UITextField!
    @IBOutlet var result: UILabel!
    
    @IBAction func guess(_ sender: AnyObject) {
        let randomNumber = arc4random_uniform(6)
        
        let userNumber = UInt32(userInput.text!)!
        
        if userNumber == randomNumber {
            result.text = "You're right!"
        }
        else {
            result.text = "You're wrong! Guess again!"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

